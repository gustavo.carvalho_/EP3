class CreateCars < ActiveRecord::Migration[5.1]
  def change
    create_table :cars do |t|
      t.string :model
      t.string :color
      t.float :price
      t.integer :year
      t.text :details

      t.timestamps
    end
  end
end
