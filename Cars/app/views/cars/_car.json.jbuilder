json.extract! car, :id, :model, :color, :price, :year, :details, :created_at, :updated_at
json.url car_url(car, format: :json)
